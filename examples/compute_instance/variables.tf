# Instance
variable "project_id" {
  description = "The GCP project to use for integration tests"
  type        = string
}

variable "region" {
  description = "The GCP region to create and test resources in"
  type        = string
  default     = "us-central1"
}

variable "subnetwork" {
  description = "The subnetwork to host the compute instances in, If neither subnetwork is specified, defaults to the network default"
  default     = "default"
}

variable "num_instances" {
  description = "Number of instances to create"
}

# disk
variable "source_image" {
  description = "Source disk image. If neither source_image nor source_image_family is specified, defaults to the latest public CentOS image."
  default     = ""
}